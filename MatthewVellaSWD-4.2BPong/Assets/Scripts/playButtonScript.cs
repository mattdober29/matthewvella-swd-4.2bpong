﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class playButtonScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        //to press play button
        GetComponent<Button>().onClick.AddListener(() => playButtonPressed());
	}

    void playButtonPressed()
    {
        //first level scene
        SceneManager.LoadScene("level1");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
