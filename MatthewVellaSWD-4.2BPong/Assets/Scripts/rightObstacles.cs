﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rightObstacles : MonoBehaviour {

    public float rightObstacleSpeed = 1f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(0, rightObstacleSpeed * Time.deltaTime , 0);
        if (transform.position.y > Camera.main.orthographicSize)
        {
            transform.position = new Vector3(transform.position.x, -Camera.main.orthographicSize);
        }

        if (transform.position.y < -Camera.main.orthographicSize)
        {
            transform.position = new Vector3(transform.position.x, Camera.main.orthographicSize);
        }
    }
}
