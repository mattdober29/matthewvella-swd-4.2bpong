﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class leftObstacles : MonoBehaviour {

    public float leftObstacleSpeed = -1f;
    

    // Use this for initialization
    private void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0, leftObstacleSpeed * Time.deltaTime, 0);
        if (transform.position.y > Camera.main.orthographicSize)
        {
            transform.position = new Vector3(transform.position.x, -Camera.main.orthographicSize);
        }

        if (transform.position.y < -Camera.main.orthographicSize)
        {
            transform.position = new Vector3(transform.position.x, Camera.main.orthographicSize);
        }
    }
}









