﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player1Script : MonoBehaviour {

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float speed = 10.0f;
       
        var move = new Vector3(0, Input.GetAxis("Vertical"), 0);
        transform.position += move * speed * Time.deltaTime;
        Vector3 clampedPosition = transform.position;
        clampedPosition.y = Mathf.Clamp(transform.position.y, -4.0f, 4.0f);
        transform.position = clampedPosition;


    }
}
