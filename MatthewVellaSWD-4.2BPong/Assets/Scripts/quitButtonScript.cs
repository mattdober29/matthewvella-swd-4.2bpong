﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class quitButtonScript : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        //The quit button is pressed
        gameObject.GetComponent<Button>().onClick.AddListener(() => quitButtonPressed());
    }

    void quitButtonPressed()
    {
        //Quits the game
        Application.Quit();
        Debug.Log("Quit");  
    }

    // Update is called once per frame
    void Update()
    {

    }
}
