﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ballScript : MonoBehaviour
{
    
    GameObject endGameScript;
    public float speed = 0f;
    public int bouceNumber = 0;
    public int gameLevelAt = 0;
    scoreController myScorecontroller;
    int playersReady = 0;
    int player1ScoreForLevel, player2ScoreforLevel;
    public bool clickEnabled, spaceEnabled = true;
    int scoreForLevel;
    public int p1ScoreForAllLevels, p2ScoreForAllLevels;
    
    // Use this for initialization
    void Start()
    {

        myScorecontroller = GameObject.Find("keepScore").GetComponent<scoreController>();

        switch (gameLevelAt)
        {
            case 0:
                speed = 8f;
                scoreForLevel = 3;      
                break;
            case 1:
                speed = 10f;
                scoreForLevel = 5;
                break;
            case 2:
                speed = 11f;
                scoreForLevel = 7;
                break;
        }
    }

    void CheckForReady()
    {
        if (playersReady == 2)
        {
            StartGame();
        }
    }

    void StartGame()
    {
        float sx = Random.Range(0, 2);
        if (sx == 0)
        {
            sx = -1;
        }
        else
        {
            sx = 1;
        }
        float sy = Random.Range(0, 2);
        if (sy == 0)
        {
            sy = -1;
        }
        else
        {
            sy = 1;
        }

        GetComponent<Rigidbody2D>().velocity = new Vector3(speed * sx, speed * sy);
    }
    void checkIfWinner()
    {
        if(player1ScoreForLevel == scoreForLevel || player2ScoreforLevel == scoreForLevel)
        {
            
            myScorecontroller.player2Score += player2ScoreforLevel;
            myScorecontroller.player1Score += player1ScoreForLevel;
            switch (gameLevelAt)
            {
                case 0:
                    SceneManager.LoadScene("level2");
                    Debug.Log(player1ScoreForLevel + " " +player2ScoreforLevel);
                    break;
                case 1:
                    SceneManager.LoadScene("level3");
                    Debug.Log(player1ScoreForLevel + " " + player2ScoreforLevel);
                    break;
                case 2:
                    SceneManager.LoadScene("endGame");
                    Debug.Log(player1ScoreForLevel + " " + player2ScoreforLevel);
                    break;
            }
            
        }
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        bouceNumber++;
        if (bouceNumber == 1)
        {
            float randomX = Random.Range(-0.2f, 0.5f);

            GetComponent<Rigidbody2D>().velocity += new Vector2(randomX, 0f);
            bouceNumber = 0;
        }
        if(collision.gameObject.tag == "leftB")
        {

            player2ScoreforLevel++;
            
            GameObject.Find("Player2Score").GetComponent<Text>().text = player2ScoreforLevel.ToString();
            
            transform.position = new Vector3(0, 0, 0);
            checkIfWinner();
        }
        if (collision.gameObject.tag == "rightB")
        {
            player1ScoreForLevel++;
            
            GameObject.Find("Player1Score").GetComponent<Text>().text = player1ScoreForLevel.ToString();
            
            transform.position = new Vector3(0, 0, 0);
            checkIfWinner();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
           
            if (GameObject.Find("P1ReadyText") != null && spaceEnabled)
            {
                spaceEnabled  = false;
                Destroy(GameObject.Find("P1ReadyText"));
                playersReady++;
                CheckForReady();
            }
        }
        else if (Input.GetMouseButtonDown(0))
        {
            
            if (GameObject.Find("P2ReadyText") != null)
            {

                Destroy(GameObject.Find("P2ReadyText"));
                playersReady++;
                CheckForReady();
            }

        }
    }
}