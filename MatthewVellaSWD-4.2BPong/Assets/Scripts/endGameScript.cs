﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class endGameScript : MonoBehaviour {

    scoreController myScorecontroller;


    //.GetComponent<ballScript>().finalScoreP1
    // Use this for initialization
    void Start () {
        myScorecontroller = GameObject.Find("keepScore").GetComponent<scoreController>();
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("endGame")) {
            int p1Score = myScorecontroller.GetComponent<scoreController>().player1Score;
            int p2Score = myScorecontroller.GetComponent<scoreController>().player2Score;
            if (p1Score > p2Score)
            {
                GameObject.Find("winnerText").GetComponent<Text>().text = ("Player 1 wins " + p1Score + " points\n\n player 2 " + p2Score + " points");
            }
            else if(p2Score > p1Score)
            {
                GameObject.Find("winnerText").GetComponent<Text>().text = ("Player 2 Wins " + p2Score + " points\n\n player 1 " + p1Score + " points");
            }
            else
            {
                GameObject.Find("winnerText").GetComponent<Text>().text = ("The game ended in a draw");
            }
        }       
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
